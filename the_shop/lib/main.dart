import 'package:flutter/material.dart';
import 'package:the_shop/screens/profile.dart';
import 'package:the_shop/screens/shopingBag.dart';
import 'package:the_shop/screens/catalog_screen.dart';
import 'package:the_shop/screens/favorite_screen.dart';
import 'package:the_shop/screens/splashScreen.dart';
import 'package:the_shop/screens/show_case_screen.dart';
import 'package:the_shop/widgets/appbar_widget.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/vitrina': (context) => const HomePage(),
        '/catalog': (context) => const CatalogPage(),
        '/korzina': (context) => const ShopingBagPage(),
        '/favorites': (context) => const FavoritePage(),
        '/profile': (context) => const ProfilePage(),
      },
      color: Colors.white,
      home: SplashScreen(),
    );
  }
}





