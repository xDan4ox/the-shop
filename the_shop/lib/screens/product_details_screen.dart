import 'package:flutter/material.dart';
import 'package:the_shop/widgets/appbar_widget.dart';
import 'package:flutter_svg/svg.dart';
import 'package:the_shop/widgets/product_card_open_widget.dart';
import 'package:the_shop/widgets/descr_product_widget.dart';
import 'package:the_shop/service/products_service.dart';

class ProductCardPage extends StatefulWidget {
  Products product;

  ProductCardPage({Key? key, required this.product}) : super(key: key);

  @override
  State<ProductCardPage> createState() => _ProductCardPageState();
}

class _ProductCardPageState extends State<ProductCardPage> {
  bool isLiked = false;

  void _onLikeChanged() {
    setState(() {
      print('Общее количество активных лайков: ');
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Material(
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: ShopAppBar(
              textBar: widget.product.name,
              haveVector: true,
              haveLink: true,
              onBackTap: () {
                Navigator.pop(context);
              },
            ),
            body: ListView(
              scrollDirection: Axis.vertical,
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 15, right: 15, bottom: 8),
                  height: 24,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.product.sale != null ? '${widget.product.sale}%' : '',
                        style: const TextStyle(
                          color: Color(0xFFB8B8B8),
                          fontSize: 18,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w400,
                          height: 1.2,
                        ),
                      ),
                      SizedBox(
                        width: 34,
                        height: 28,
                        child: SvgPicture.asset('assets/red_heart.svg'),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: ProductCardOpen(name: widget.product.name, price: widget.product.price, oldPrice: widget.product.oldPrice, image: widget.product.photo),
                ),
                const Delimiter(),
                DescrProduct(
                  GeneralDescr:
                      'Стильный столик журнальный металлический с подносом в стиле лофт эффектно впишется в тематический дизайн интерьера.',
                  Style: 'Лофт',
                  Kind: 'Журнальный',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
