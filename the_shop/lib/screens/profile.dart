import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:the_shop/screens/address_page_screen.dart';
import 'package:the_shop/screens/authorized_profile_screen.dart';
import 'package:the_shop/widgets/custom_button_widget.dart';
import '../widgets/appbar_widget.dart';
import '../widgets/downbar_widget.dart';
import 'profile_details_screen.dart';


class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Material(
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: const ShopAppBar(textBar: 'Профиль'),
            body: Column(
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(top: 16),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(
                              bottom: 24, top: 16, left: 15, right: 15),
                          child:
                              GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => const AutorizedProfile()),
                                    );
                                  },
                                  child: const CustomButton(text: 'войти / зарегистрироваться')),
                        ),
                        ProfileItem(text: 'Адреса магазинов', context: context,)
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 16, bottom: 4),
                  child: DownBar(indxPage: 5, parentContext: context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileItem extends StatelessWidget {
  String text;
  BuildContext context;
  final int diffPage;

  ProfileItem({Key? key, this.text = 'example', required this.context, this.diffPage = 0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          diffPage == 1 ? MaterialPageRoute(builder: (context) => const MyProfile()) : MaterialPageRoute(builder: (context) => const AdressPage()) ,
        );
      },
      child: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 15),
                  height: MediaQuery.of(context).size.height * 0.07,
                  child: Text(
                    text,
                    style: const TextStyle(
                      color: Color(0xFF1F1F1F),
                      fontSize: 16,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,
                      height: 2.5,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 15),
                  height: MediaQuery.of(context).size.height * 0.07,
                  child: Container(
                      width: 30,
                      height: 30,
                      child: Image.network('https://icons.veryicon.com/png/o/system/system-3/arrow-right-s-line-1.png')),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 15),
              decoration: const ShapeDecoration(
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    width: 1,
                    strokeAlign: BorderSide.strokeAlignCenter,
                    color: Color(0xFFE3E3E3),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

